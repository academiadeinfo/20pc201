#include <iostream>
#include <fstream>

using namespace std;

string nume[100]; /// sir separat pentru nume.
int note[100][100]; ///100 linii și 100 coloane
int n, m;

void citire()
{
    ifstream f ("concurs.in");
    f >> n >> m;
    ///citesc numele participanților
    for(int i = 1; i <= m; i++)
        f >> nume[i];

    ///citesc notele
    for(int i = 1; i <= n; i++)
        for(int j = 1; j <= m; j++)
            f >> note[i][j];
}

void afisare()
{
    for(int i = 1; i <= m; i++)
        cout << nume[i] << "\t";
    cout << endl;
    for(int i = 1; i <= n; i++)
    {
        for(int j = 1; j <= m; j++)
            cout << note[i][j] << "\t";
        cout << endl;
    }
}

///parcurgerea unei coloane

///returnează suma elementelor de pe coloana k
int sumacol(int k)
{
    int suma = 0;
    for(int i = 1; i <= n; i++) /// pentru fiecare linie i
        suma += note[i][k];
    return suma;
}

///rezolvare
void rezolvare()
{
    int maxim = 0;
    ///parcurgem fiecare coloană (artistii) (m coloane)
    for(int i = 1; i <= m; i++)
        if(sumacol(i) > maxim)
            maxim = sumacol(i);
    cout << "\nmaxim:\t\t" << maxim <<endl;
    cout << "castigatori:\t";

    for(int i = 1; i <= m; i++)
        if(sumacol(i) == maxim)
            cout << nume[i] << ", ";
    cout << endl;
}

int main()
{
    citire();
    afisare();
    rezolvare();
    return 0;
}
