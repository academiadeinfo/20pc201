#include <iostream>

using namespace std;

///utilizatorul va introduce numele unei piese
///și coordonatele acesteia
///se va marca cu X pe matrice locurile unde poate muta

char tb[10][10];
int n = 8;

void curatare()
{
    for(int i = 1; i <= n; i++)
        for(int j = 1; j <= n; j++)
            tb[i][j] = '-';
}

void afisare()
{
    for(int i = 1; i <= n; i++, cout << endl)
        for(int j = 1; j <= n; j++)
            cout << " " << tb[i][j];
}

void pion(int x, int y)
{
    tb[x+1][y] = 'X';
    if(x == 2)
        tb[x+2][y] = 'X';
}

void tura(int x, int y)
{
    for(int i = 1; i <= n; i++)
    {
        tb[x][i] = 'X';
        tb[i][y] = 'X';
    }
}

void nebun(int x, int y)
{
    for(int i = 1; x+i <= n && y+i <=n; i++)
        tb[x+i][y+i] = 'X';
    for(int i = 1; x-i > 0 && y-i > 0; i++)
        tb[x-i][y-i] = 'X';
    for(int i = 1; x+i <= n && y-i > 0; i++)
        tb[x+i][y-i] = 'X';
    for(int i = 1; x-i > 0 && y+i <=n; i++)
        tb[x-i][y+i] = 'X';
}

void generare(string piesa, int x, int y)
{
    if(piesa == "pion" or piesa == "p" or piesa == "P")
        pion(x, y);
    if(piesa == "tura" or piesa == "t" or piesa == "T")
        tura(x, y);
    if(piesa == "nebun" or piesa == "n" or piesa == "N")
        nebun(x, y);
    if(piesa == "dama" or piesa == "d")
    {
        tura(x, y);
        nebun(x, y);
    }
    tb[x][y] = toupper(piesa[0]);
}

int main()
{
    string piesa;
    int x, y;
    while(true)
    {
        cin >> piesa >> x >> y;
        curatare();
        generare(piesa, x, y);
        afisare();
    }
    return 0;
}
