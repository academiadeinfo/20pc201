#include <iostream>

using namespace std;

int main()
{
    ///int a[100][100]; /// sirul a cu maxim 100 linii si 100 coloane
    ///a[x][y]; /// elementul de pe linia x, coloana y

    ///Generarea unei matrice:
    ///să se genereze o matrice cu 5 linii si 10 coloane,
    ///unde fiecare element este indicele liniei sale

    int mat[20][20], n = 5, m = 10;

    for(int i = 1; i <= n; i++)
        for(int j = 1; j <= m; j++)
            mat[i][j] = i + j;

    ///Afișarea unei matrice
    for(int i = 1; i <= n; i++)
    {
        for(int j = 1; j <= m; j++)
            cout << mat[i][j] << "\t";
        cout << endl;
    }


    return 0;
}
