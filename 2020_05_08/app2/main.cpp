#include <iostream>
#include <fstream>

using namespace std;

///din fisierul mat.in se citeste o matrice
///în consolă se afișează matricea

///la infinit:
///se citește un număr q
///se afișează câte elemente pare sunt pe linia q

int n, m;
int a[100][100];

void citire()
{
    ifstream fin ("mat.in");
    fin >> n >> m;
    for(int i = 1; i <= n; i++)
        for(int j = 1; j <= m; j++)
            fin >> a[i][j];
}

void afisare()
{
    for(int i = 1; i <= n; i++, cout << endl)
        for(int j = 1; j <= m; j++)
            cout << a[i][j] << "\t";
    cout << endl;
}

///parcurgerea unei linii;
int parelin(int lin)
{
    int c = 0;
    for(int j = 1; j <= m; j++) ///iau fiecare coloană j
        if(a[lin][j] % 2 == 0) /// fac ceva cu a[lin][j]
            c++;
    return c;
}

int main()
{
    citire();
    afisare();
    while(1)
    {
        int q;
        cout << "linia: ";
        cin >> q;
        cout << "pare: " << parelin(q) << "\n\n";
    }
    return 0;
}
