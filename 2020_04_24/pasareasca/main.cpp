#include <iostream>

using namespace std;

bool eVocala(char c)
{
    string vocale = "AEIOUYaeiouy";
    if(vocale.find(c) == -1)
        return false;
    else
        return true;
}

string codare(string original)
{
    string codat = "";
    int n = original.length();
    for(int i = 0; i < n; i++)
    {
        codat += original[i];
        if(eVocala(original[i]))
            codat = codat + "p" + original[i];
    }
    return codat;
}

string decodare(string codat)
{
    string s = codat;
    for(int i = 0; i < s.length(); i++)
        if(eVocala(s[i]))
            s.erase(i+1, 2);
    return s;
}

int main()
{
    string s;
    getline(cin, s);
    cout << codare(s) << endl;
    getline(cin, s);
    cout << decodare(s) << endl;
    return 0;
}
