#include <iostream>
#include <fstream>

using namespace std;

string dictRO[2048], dictEN[2048];
int nd = 0;

string lastWord = "";
int vieti = 3;


void readDict()
{
    ifstream f ("dictionar.txt");
    int aux;
    while(f >> aux >> dictRO[nd])
    {
        getline(f, dictEN[nd]);
        nd++;
    }
}

bool correct(string word)
{
    if(lastWord == "")
        return true;
    if(word.length() < 3)
        return false;
    string prime = word.substr(0, 2); ///extrag primele 2 litere
    int lastL = lastWord.length();
    string ultime = lastWord.substr(lastL-2, 2);
    if(prime == ultime)
        return true;
    return false;
}


string searchWord()
{
    for(int i = 0; i < nd; i++)
        if(correct(dictRO[i]))
            return dictRO[i];

    return "-1";
}

int main()
{
    readDict();
    while(vieti != 0)
    {
        string word;
        cout << "introduce cuvant: ";
        cin >> word;
        if(correct(word))
        {
            lastWord = word;
            string npc = searchWord();
            if(npc != "-1")
            {
                cout << npc << endl;
                lastWord = npc;
            }
            else
            {
                cout << "Oups, m-ai inchis: RESET" << endl;
                lastWord = "";
            }
        }
        else
        {
            cout << "Gresit. Reset." << endl;
            vieti--;
            lastWord = "";
        }
    }
    return 0;
}
