#include <iostream>

using namespace std;

//https://docs.google.com/document/d/1O_Li8o8FFgIFtYo8ChnD8----B-6hhxOmXtkcjqNdNU/edit?usp=sharing
//p2

int a[1024], na;

void citire()
{
    cin >> na;
    for(int i = 1; i <= na; i++)
        cin >> a[i];
}

bool conditie(int x)
{
    /// verific dacă x e divizibil cu 3 sau are ultima cifra 3
    return x % 3 == 0 || x % 10 == 3;
}

int rezolvare()
{
    ///verific câte elemente din șir îdeplinesc condiția
    int contor = 0;
    for(int i = 1; i <= na; i++)
        if( conditie(a[i]) )
            contor++;
    return contor;
}

int main()
{
    citire();
    cout << rezolvare();
    return 0;
}
