#include <iostream>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include <conio.h>

using namespace std;

int cod(char c)
{
    if(c == 'r' || c == 'R')
        return 0;
    if(c == 'p' || c == 'P')
        return 1;
    if(c == 's' || c == 'S')
        return 2;
    return -1;
}

string decod(int c)
{
    if(c == 0)
        return "Rock";
    if(c == 1)
        return "Paper";
    if(c == 2)
        return "Scissors";
}

int punctaj(int player, int npc)
{
    if(player == npc)
        return 0;
    else if(player == 0 && npc == 2)
        return 1;
    else if(player == 1 && npc == 0)
        return 1;
    else if(player == 2 && npc == 1)
        return 1;
    else
        return -1;
}

void printfile(string file)
{
    ifstream f (file.c_str());
    char c;
    while(f >> noskipws >> c)
        cout << c;
    cout << endl;
}

void printchoice(int x)
{
    if(x == 0)
        printfile("rock.asc");
    else if(x == 1)
        printfile("paper.asc");
    else if(x == 2)
        printfile("scissors.asc");
}

void afisare_rezultat(int p)
{
    if(p == -1)
        cout << "ai pierdut" << endl;
    else if(p == 0)
        cout << "egalitate" << endl;
    else
        cout << "Ai castigat!" << endl;
}

int runda(char choice)
{
    cout << endl;
    int player = cod(choice);
    if(player == -1)
    {
        cout << "tasta incorecta" << endl;
        return 0;
    }
    int npc = rand() % 3;

    cout << "Player: \n";
    printchoice(player);
    cout << "Calculator: \n";
    printchoice(npc);

    int p = punctaj(player, npc);
    afisare_rezultat(p);
    return p;
}

int main()
{
    srand(time(NULL));
    int scor = 0;
    char choice;
    cout << "scor: " << scor << endl << endl;
    cout << "Ce alegeti? (R / P / S): ";
    while(choice != 'q')
    {
        choice = getch();
        system("cls");
        cout << "scor: " << scor << endl;
        scor += runda(choice);
        cout << "\nCe alegeti? (R / P / S): ";
    }
    return 0;
}
