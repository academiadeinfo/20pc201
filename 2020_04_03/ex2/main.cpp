#include <iostream>

using namespace std;

/**
Se citește un număr natural n urmat de n numere naturale.
Să se afișeze diferența dintre suma numerelor pare și suma numerelor impare din șir.
*/

int a[1024], na;

void citire()
{
    cin >> na;
    for(int i = 1; i <= na; i++)
        cin >> a[i];
}

int sumarest(int r)
{
    ///cand r este 0 face suma elementelor pare
    ///cand r este 1 face suma elementelor impare
    int s = 0;
    for(int i = 1; i <= na; i++)
        if(a[i] % 2 == r)
            s += a[i];
    return s;
}


int main()
{
    citire();
    cout << "suma pare: " << sumarest(0) << endl;
    cout << "suma impare: " << sumarest(1) << endl;
    cout << "diff: " << sumarest(0) - sumarest(1);

    return 0;
}
