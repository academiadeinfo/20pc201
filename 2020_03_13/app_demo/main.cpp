#include <iostream>
#include <conio.h>

using namespace std;

void calculator()
{
    double a, b;
    char semn;
    cout << endl << ": ";
    cin >> a >> semn >> b;
    if(semn == '+')
        cout << " = " << a + b << endl;
    else if(semn == '-')
        cout << " = " << a - b << endl;
    else if(semn == '/')
        cout << " = " << a / b << endl;
    else if(semn == '*')
        cout << " = " << a * b << endl;
    else if(semn == '^')
    {
        int p = 1;
        for(int i = 1; i <= b; i++)
            p*=a;
        cout << p << endl;
    }
}

int main()
{
    char c = 'Y';
    while(c != 'N' && c != 'n')
    {
        calculator();
        cout << "continuati? (Y / N) ";
        c = getch();
    }

    return 0;
}
