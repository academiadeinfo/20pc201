#include <iostream>
#include <fstream>

using namespace std;

int sir[1024], n;

///functie care citeste sirul din fisier
void citire()
{
    ifstream f ("sir.in");
    f >> n;
    for(int i = 1; i <= n; i++)
        f >> sir[i];
}
///funcție care afișează șirul
void afisare()
{
    for(int i = 1; i <= n; i++)
        cout << sir[i] << "\t";
    cout << endl;
}
///funcție care afișează suma numerelor din șir
void suma()
{
    int s = 0;
    for(int i = 1; i <= n; i++)
        s += sir[i];
    cout << "suma: " << s << endl;
}
///funcție care afișează maximul din sir
void maxim()
{
    int maxim = sir[1];
    for(int i = 1; i <= n; i++)
        if(sir[i] > maxim)
            maxim = sir[i];
    cout << "maxim: " << maxim << endl;
}

///funcție care înlocuiește numerele pare cu rezultatul împărțirii lor la 2
void modificare()
{
    for(int i = 1; i <= n; i++)
        if(sir[i] % 2 == 0) //dacă numărul este par
            sir[i] /= 2; // îl înlocuiesc cu câtul împărțirii lui la 2
}

int main()
{
    citire();
    afisare();
    suma();
    maxim();
    modificare();
    afisare();
    suma();
    maxim();

    return 0;
}
