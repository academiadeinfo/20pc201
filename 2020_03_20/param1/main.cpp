#include <iostream>

using namespace std;

///funcție care afișează suma a 2 int-uri
void print_sum(int x, int y)
{
    cout << x + y << endl;
}

int main()
{
    ///Se citesc numerele naturale a, b, c
    int a, b, c;
    cin >> a >> b >> c;
    print_sum(a, b);
    print_sum(b, c);
    print_sum(a, c);

    return 0;
}
