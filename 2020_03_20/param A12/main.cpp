#include <iostream>

using namespace std;

///A1.2. Se citesc numerele naturale a și b.
///Câte cifre are a? Dar b?

///funcție care primește un parametru int
///și returnează numărul lui de cifre
int nrcif(int x)
{
    int cifre = 0; //contor care numără cifrele
    while(x != 0)//cât timp x mai are cifre
    {
        cifre++; // cresc numărul de cifre
        x = x / 10; // tai ultima cifră a lui
    }
    return cifre; // returnez rezultatul
}

int main()
{
    int a, b;
    cin >> a >> b;
    cout << "cifre a: " << nrcif(a) << endl;
    cout << "cifre b: " << nrcif(b) << endl;
    return 0;
}
