#include <iostream>
#include <fstream>

using namespace std;

int a[1024], na;

void citire()
{
    ifstream fin ("recap.in");
    fin >> na;
    for(int i = 1; i <= na; i++)
        fin >> a[i];
}

void afisare()
{
    for(int i = 1; i <= na; i++)
        cout << a[i] << " ";
    cout << endl;
}

void catepare()
{
    int cate = 0;
    for(int i = 1; i <= na; i++)
        if(a[i] % 2 == 0)
            cate++;
    cout << "pare: " << cate << endl;
}

void inlocuire()
{
    ///Înlocuiți toate numerele cu produsul ultimelor
    ///lor 2 cifre.
    for(int i = 1; i <= na; i++)
        a[i] = (a[i] % 10) * (a[i]/10%10);
}

void sumaimpare()
{
    int suma = 0;
    for(int i = 1; i <= na; i++)
        if(a[i] % 2 != 0)
            suma += a[i];
    cout << "suma imp: " << suma << endl;
}

int main()
{
    citire();
    afisare();
    catepare();
    sumaimpare();
    inlocuire();
    afisare();
    catepare();
    sumaimpare();

    return 0;
}
