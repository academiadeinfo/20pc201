#include <iostream>

using namespace std;

/**
La școală, Elif a învățat că poate calcula cât timp durează un drum dacă împarte distanța la viteză. Întrebând, el află că până la bunici este o distanță de d kilometri și mașina se deplasează cu o viteză de v kilometri pe oră. În timpul drumului, mașina face pană și mai durează p ore până se repară.
    Se citesc numerele d, v, și p   de tip real (double)
    Câte ore durează călătoria?

*/

int main()
{
    double v, d, p;
    cout << "distanta:\t";
    cin >> d;
    cout << "viteza: \t";
    cin >> v;
    cout << "timp de pana:\t";
    cin >> p;

    cout << "\ntimp total:\t" << d / v + p << " ore\n";
    return 0;
}
