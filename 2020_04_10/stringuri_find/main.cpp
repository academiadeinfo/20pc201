#include <iostream>

using namespace std;

/**
Se citeste un text.
Se cauta toate aparițile cuvantului "elsa" in text
și se înlocuiește prima literă în majusculă ('E' )
*/

int main()
{
    string text;// = "elsa are mere, pere, gutui dar elsa a ramas fara banane.";

    getline(cin, text); ///se citește o linie întreagă din CIN

    int poz = text.find("elsa"); ///caut cuvantul in text
    while(poz != -1) ///cat timp am gasit cuvantul in text
    {
        cout << "gasit pe pozitia: " << poz << endl; ///afișez poziția
        text[poz] = 'E'; ///schimb prima litera
        cout << text << endl; //afișez textul modificat
        poz = text.find("elsa"); ///caut următoarea apariție
    }

    return 0;
}
