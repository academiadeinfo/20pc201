#include <iostream>

using namespace std;

/**
Când ajunge acasă, bunicul îl trimite pe Elif să dea mâncare la animale. În ogradă sunt v vaci, g găini, p porci.
Fiecare vacă mănâncă 1 găleată de mâncare.
Dacă sunt mai puțin de 10 găini ele vor mânca împreună 1 găleată de mâncare. Dacă nu, va fi nevoie de 2 găleți.
Dacă sunt mai mulți porci decât vaci ei mănâncă 1 găleată fiecare. Altfel, vor mânca 2 găleți fiecare.
Se citesc numerele v, g şi p
De câte găleți de mâncare are nevoie Elif ?

*/

int main()
{
    int vaci, gaini, porci;
    cout << "vaci:\t";
    cin >> vaci;
    cout << "gaini: \t";
    cin >> gaini;
    cout << "porci:\t";
    cin >> porci;

    int mancare = 0; //la inceput am nevoie de 0 galeti

    mancare = mancare + vaci; //adaug cate 1 pentru fiecare vacă
    if(gaini < 10) //dacă sunt mai puțin de 10 gaini
        mancare = mancare + 1; //adaug o găleată
    else //altfel
        mancare = mancare + 2; //adaug 2 găleți

    if(porci > vaci) //dacă sunt mai mulți porci decât vaci
        mancare = mancare + porci; // adaug 1 pentru fiecare porc
    else //altfel
        mancare = mancare + 2*porci; //adaug 2 pentru fiecare porc (2*porci)

    cout << "\nE nevoie de " << mancare << " galeti de mancare\n"; //afișez totalul

    return 0;
}
