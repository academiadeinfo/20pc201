#include <iostream>

using namespace std;

void vocale1()
{
    ///se introduc caractere până la caracterul q.
    ///verificați pentru fiecare dacă este vocală
    string vocale = "aeiouAEIOU";
    char c;
    while(c != 'q')
    {
        cin >> c;
        if(vocale.find(c) == -1)
            cout << "nu e vocala"<<endl;
        else
            cout << "este vocala"<<endl;
    }
}

int cateVocale(string cuv)
{
    ///functie care returnează câte vocale conține cuvantul
    string vocale = "aeiouAEIOU";
    int c = 0;
    for(int i = 0; i < cuv.length(); i++)
        if(vocale.find(cuv[i]) != -1)
            c++;
    return c;
}

int main()
{
    //vocale1();

    ///se citeste un text, sa se afiseze cate vocale sunt in text
    string s;
    getline(cin, s);
    cout << "nr de vocale: " << cateVocale(s) <<endl;

    return 0;
}
