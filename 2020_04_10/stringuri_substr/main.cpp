#include <iostream>

using namespace std;

///	.substr(int a, int n) -> returnează portiunea din string de la poziția a, n caractere


int main()
{
    string s = "A fost o data in fermecata lume a codarniei...";

    cout  << s.substr(5, 12) << endl;

    int start = s.find("fermecata");
    int n = s.find('.') - start;
    cout << s.substr(start, n) << endl;

    return 0;
}
