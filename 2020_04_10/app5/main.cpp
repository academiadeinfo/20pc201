#include <iostream>

using namespace std;

/**
Elsa o ajută pe bunica să facă plăcinte. În rețetă scrie că plăcintele trebuie lăsate la cuptor între a minute și b minute. După ce pune o plăcintă la cuptor, Elsa merge afară să se joace. Când se întoarce, vede că au trecut t minute de când a plecat. Cum este plăcinta?
Se citesc numerele a, b, t.
Să se afișeze necoaptă dacă a trecut prea puțin timp, perfectă dacă t este între a și b, arsă dacă a trecut prea mult timp.
*/

int main()
{
    int a, b, t;
    cout << "timp minim: ";
    cin >> a;
    cout << "timp maxim: ";
    cin >> b;
    cout << "cat timp a stat? ";
    cin >> t;
    cout << endl;

    if(t < a) //dacă a stat mai puțin de timpul minim
        cout << "necoapta" << endl;//atunci e necoapta
    else if (t > b) //altfel(dacă nu e necoaptă) și a stat mai mult de timpul maxim
        cout << "arsa" << endl; //atunci este arsa
    else //altfel (dacă nu e nici necoaptă nici arsă
        cout << "perfecta" << endl; // e perfectă

    return 0;
}
