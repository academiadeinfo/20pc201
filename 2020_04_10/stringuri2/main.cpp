#include <iostream>

using namespace std;

int main()
{
    string cuv;

    cin >> cuv;

    cout << "ai introdus: " << cuv << endl;

    cout << "prima litera este: " << cuv[0] << endl;
    cout << "a doua litera este: " << cuv[1] << endl;

    cout << "\nAre " << cuv.length() << " litere:\n";

    for(int i = 0; i < cuv.length(); i++)
        cout << cuv[i] << " ";
    cout << endl;

    ///Provocare: se citește o literă. De câte ori apare litera în cuv
    char c;
    int contor = 0;
    cout << "introduceti un caracter: ";
    cin >> c;

    for(int i = 0; i < cuv.length(); i++)
        if(cuv[i] == c)
            contor++;
    cout << "caracterul " << c << " apare de " << contor << " ori.\n";

    return 0;
}
