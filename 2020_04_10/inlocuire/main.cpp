#include <iostream>

using namespace std;

int main()
{
    ///se citește un text, si doua cuvinte: p și q
    /// să se înlocuiască toate aparitiile lui p cu q

    string text, p, q;
    cout << "introduceti textul: ";
    getline(cin, text); ///citesc textul - o linie intreaga - din consola
    cout << "inlocuiti cuvantul: ";
    cin >> p;
    cout << "cu cuvantul: ";
    cin >> q;

    int poz = text.find(p);///caut prima aparitie a lui p in text
    while(poz != -1) ///daca am gasit o aparitie a lui p in text
    {
        text.erase(poz, p.length()); ///sterg de la pozitia respectiva, numarul de caractere ale lui p
        text.insert(poz, q); ///inserez la aceeași poziție cuvantul q
        poz = text.find(p); ///caut următoarea apariție
    }

    cout << endl << text << endl; //afișez textul rezultat

    return 0;
}
