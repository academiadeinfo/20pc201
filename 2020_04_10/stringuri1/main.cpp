#include <iostream>
#include <cstring>

using namespace std;

int main()
{
    char s[100];

    cin >> s;
    cout << "ai introdus: " << s << endl;

    int n = strlen(s); ///cate elemente are stringul

    for(int i = 0; i < n; i++)
        cout << s[i] << " ";

    cout << endl;

    if(strcmp(s, "cuvant") == 0)
        cout << "ati introdus 'cuvant' " << endl;

    return 0;
}
