#include <iostream>
#include <cstdlib>
#include <conio.h>
#include <ctime>
#include <fstream>

using namespace std;

string cuvant; ///cuvântul corect
string ghicit; /// ce știe jucătorul
int vieti;
bool castigat;

int nd; ///numărul de cuvinte din dictionar
string dictionar[1024]; /// cuvintele
string vechi;

int puncte = 0;

void inlocuiesc(char lit); ///prototipul funcției -> am undeva functia asta


bool isLetter(char c)
{
    if(c >= 'a' && c <= 'z')
        return true;
    if(c >= 'A' && c <= 'Z')
        return true;
    return false;
}

void init()
{
    nd = 0;
    vieti = 5;
    castigat = false;
    vechi="";
    ifstream f ("cuvinte.txt");
    while(f >> dictionar[nd])
        nd++;

    int rind = rand() % nd; ///indice random din sirul dictionar
    cuvant = dictionar[rind]; ///aleg cuvântul
    ghicit = "";
    for(int i = 0; i < cuvant.length(); i++)
        if(isLetter(cuvant[i]))
            ghicit += '_';
        else
            ghicit += cuvant[i];

    inlocuiesc(cuvant[0]); ///dezvelesc toate aparitiile primei litere
    inlocuiesc(cuvant[cuvant.length()-1]); ///idem pentru ultima literă
}

void printfile(string nume)
{
    ifstream f (nume.c_str());
    char c;
    cout << endl;
    while(f >> noskipws >> c)
        cout << c;
    cout << endl;
}

void afisez()
{
    //cout << "vieti: " << vieti << endl << endl;
    cout <<"Punctaj total: " << puncte << endl;
    string ss = "vieti";
    ss += '0'+vieti;
    printfile(ss);
    for(int i = 0; i < ghicit.length(); i++)
        cout << ghicit[i] << " ";
    cout << endl << endl;
    cout << vechi << endl;
}

void inlocuiesc(char lit)
{
    ///înlocuiesc toate apartiile literei găsite în stringul ghicit
    for(int i = 0; i < cuvant.length(); i++)
        if(toupper(cuvant[i]) == toupper(lit)) /// sau tolower()
            ghicit[i] = cuvant[i];
}

void rezultat(char lit)
{
    if(cuvant.find(lit) != -1 ) ///dacă am găsit litera
        cout << "bravo, ai gasit o litera!" << endl << endl;
    else
    {
        vieti--;
        cout << "oups, nu exista litera " << (char)toupper(lit) << ", ";
        cout << "pierzi o viata" << endl << endl;
    }
    if(ghicit == cuvant)
    {
        cout << "GOOD JOB! YOU WIN (^_^)" << endl;
        printfile("win");
        puncte++;
        castigat = true;
    }
    if(vieti == 0)
    {
        afisez();
        cout << "GAME OVER, YOU LOSE  (x_X)" << endl;
        cout << "Word: " << cuvant << endl;
        puncte = 0;
    }
}

int main()
{
    srand(time(NULL)); /// pregătim seed-ul de random
    char again = 'y';
    while(again == 'y' || again == 'Y')
    {
        system("cls");
        init(); /// initializez jocul
        while(vieti > 0 && !castigat)
        {
            afisez();
            char c;
            cout << "introduce litera";
            c = getch(); /// cin >> c; dar nu mai trebuie enter
            system("cls");
            vechi = vechi + c + " ";
            inlocuiesc(c);
            rezultat(c);
        }
        cout << endl << "play again? (y/n)";
        again = getch();
    }

    return 0;
}
