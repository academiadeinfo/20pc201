#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

string dict[1024];
int nd = 0;

bool cmp(string a, string b)
{
    return a.length() < b.length();
}

int main()
{
    ifstream fin("dict.txt");
    ofstream fout("cuvinte.txt");

    string s;

    while(fin >> s)
    {
        if(s.length() > 3)
            dict[nd++] = s;
        getline(fin, s);
    }

    sort(dict, dict+nd, cmp);

    for(int i = 0; i < nd; i++)
        fout << dict[i] << endl;

    cout << nd << endl;

    return 0;
}
