#include <iostream>

using namespace std;
///https://slides.com/academiadeinformatica/deck-13#/13


int max2 (int x, int y)
{
    if(x > y)
        return x;
    else
        return y;
}

int max4 (int x, int y, int z, int w)
{
    return max2( max2(x, y) , max2(z, w) );
}

int main()
{
    int a, b, c, d;
    cout << max4(9, 8, 3, 12) << endl;
    cout << max4(9, 12, 3, 9) << endl;
    cin >> a >> b >> c >> d;
    cout << max4(a, b, c, d);

    return 0;
}
