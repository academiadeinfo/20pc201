#include <iostream>

using namespace std;

int suma(int a, int b)
{
    return a+b;
}
int diff(int a, int b)
{
    return a-b;
}
int prod(int a, int b)
{
    return a*b;
}
int minim(int a, int b)
{
    if(a < b)
        return a;
    else
        return b;
}

int rest(int x, int y)
{
    return x % y;
}

int ucif(int x)
{
    return rest(x, 10);
}

int cat(int x, int y)
{
    return x / y;
}

int main()
{
    int a, b, c;
    cin >> a >> b >> c;
    ///1. Dublul minimului dintre a și b.
    cout << "1. " << prod(2, minim(a, b)) << endl;

    ///2. Diferenta dintre produsul numerelor a, b, c și suma lor.
    cout << "2. "
      <<diff(prod(a, prod(b, c)), suma(a, suma(b, c)))
      << endl;
    ///3. Ucif c
    cout << "3. " << rest(c, 10);
    ///4. Suma ultimelor cifre ale lui a, b, c
    cout << "4. " << suma(ucif(a), suma(ucif(b), ucif(c))) << endl;
      /// suma(rest(a, 10), suma( rest(b, 10), rest(c, 10) ) )
    ///5. Penultima cifră a lui c.
    cout << "5. " << rest(cat(c, 10), 10) << endl;
      /// ucif(cat(a, 10));
    ///6. Dublul sumei dintre produsul lui a și b și produsul lui b și c
    cout << "6. " << prod(2, suma(prod(a, b), prod(b, c))) << endl;

    ///7. a + b*c - b
    cout << "7. " << diff(suma(a, prod(b, c)), b) << endl;

    ///8. a + b/2 - (a-c)
    cout << "8. " << diff(suma(a, cat(b, 2)), diff(a, c)) << endl;

    return 0;
}
