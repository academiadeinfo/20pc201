#include <iostream>

using namespace std;

///https://slides.com/academiadeinformatica/deck-13#/13/3

int sumacif(int x)
{
    int s = 0;
    while(x) // cât timp mai am cifre
    {
        s += x % 10; ///s = s + x % 10
        x /= 10;
    }
    return s;
}

///functia max2 care ia 2 nr si return maximul
int max2(int x, int y)
{
    if(x > y)
        return x;
    return y; /// else implicit
}

int max2(int x, int y, int z)
{
    return max2(x, max2(y, z));
}

int main()
{
    ///se citesc 3 nr nat. se afișează maximul dintre sumele cifrelor
    int a, b, c;
    cin >> a >> b >> c;
    //cout << max2(a, b) << endl;
    cout << max2(sumacif(a), sumacif(b), sumacif(c)) << endl;

    return 0;
}
